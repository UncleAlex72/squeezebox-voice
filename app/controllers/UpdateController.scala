package controllers

import media.MediaUpdateMediator
import play.api.Logger
import play.api.libs.json._
import play.api.mvc._
import uk.co.unclealex.diagnostics.SecurableByBearerToken

import scala.concurrent.ExecutionContext

/**
 * The controller used to send a request to synchronise DialogFlow with the SqueezeCentre.
 */
class UpdateController(
                        cc: ControllerComponents,
                        val bearerToken: String,
                        mediaUpdateMediator: MediaUpdateMediator)(implicit ec: ExecutionContext)
  extends AbstractController(cc) with SecurableByBearerToken {

  def update(): Action[AnyContent] = SecuredByBearerToken {
    Action.async { implicit request: Request[AnyContent] =>
      mediaUpdateMediator.update.map(_ => Ok(JsString("Updated"))).recover {
        case e: Exception =>
          Logger.error("An error occurred whilst updating", e)
          InternalServerError(JsString(e.getMessage))
      }
    }
  }

}
