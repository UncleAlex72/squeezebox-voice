package loader

import java.io.FileInputStream
import java.time.Clock

import akka.actor.ActorSystem
import com.typesafe.scalalogging.StrictLogging
import controllers.{AssetsComponents, UpdateController, WebhookController}
import dialogflow.{UploadEntitiesService, UploadEntitiesServiceImpl}
import hacks.{AlbumTrackReportingReversalHack, SequenceMatchingAlbumTrackReportingReversalHack}
import lexical._
import media._
import play.api.libs.json.{JsPath, Json, Reads}
import play.api.libs.ws.ahc.AhcWSComponents
import uk.co.unclealex.squeezebox.SqueezeboxPing
import play.api.routing.Router
import play.api.{ApplicationLoader, Configuration}
import router.Routes
import squeezebox._
import uk.co.unclealex.diagnostics.DiagnosticsFromContext
import uk.co.unclealex.squeezebox.{AkkaStreamMediaServerClient, MediaServer, MediaServerClient}
import webhook.{WebhookService, WebhookServiceImpl}

import scala.concurrent.Await
import scala.concurrent.duration._

class AppComponents(context: ApplicationLoader.Context)
  extends DiagnosticsFromContext(context, "squeezebox-voice")
    with AhcWSComponents
    with AssetsComponents
    with StrictLogging {

  implicit val _actorSystem: ActorSystem = actorSystem

  val clock: Clock = Clock.systemDefaultZone()

  val dialogFlowToken: String = configuration.get[String]("dialogFlow.token")
  val uploadEntitiesService: UploadEntitiesService =
    new UploadEntitiesServiceImpl(
      ws = wsClient,
      authorisationToken = dialogFlowToken,
      timeout = configuration.get[Duration]("dialogFlow.timeout"))
  val removePunctuationService: RemovePunctuationService = new RemovePunctuationServiceImpl()

  val albumTrackReportingReversalHack: AlbumTrackReportingReversalHack = hacks()
  val roomsProvider: RoomsProvider = rooms(removePunctuationService)

  val squeezeCentreLocation: SqueezeCentreLocation = new ConfiguredSqueezeCentreLocation(configuration = configuration)
  val romanNumeralsService: RomanNumeralsService = new RomanNumeralsServiceImpl()
  val synonymService: SynonymService = new RomanNumeralSynonymService(romanNumeralsService = romanNumeralsService)
  val mediaServer = MediaServer(squeezeCentreLocation.host, squeezeCentreLocation.port)
  val mediaServerClient: MediaServerClient = new AkkaStreamMediaServerClient(mediaServer)

  registerHealthCheck("squeezebox", SqueezeboxPing(mediaServer), 1.minute)

  val (musicPlayer, musicRepository): (MusicPlayer, MusicRepository) = both(
    new MusicPlayerImpl(
      mediaServerClient = mediaServerClient,
      synonymService = synonymService,
      removePunctuationService = removePunctuationService)
  )
  val (mediaCacheView, mediaCacheUpdater): (MediaCacheView, MediaCacheUpdater) = both(
    new MapMediaCache(removePunctuationService = removePunctuationService)
  )
  val mediaUpdateMediator: MediaUpdateMediator =
    new MediaUpdateMediatorImpl(
      mediaCacheUpdater = mediaCacheUpdater,
      musicRepository = musicRepository,
      uploadEntitiesService = uploadEntitiesService,
      roomsProvider = roomsProvider)

  val nowPlayingService: NowPlayingService = new NowPlayingServiceImpl(
    musicPlayer = musicPlayer,
    albumTrackReportingReversalHack = albumTrackReportingReversalHack)

  val webhookService: WebhookService = new WebhookServiceImpl(
    musicPlayer = musicPlayer,
    mediaCacheView = mediaCacheView,
    nowPlayingService = nowPlayingService,
    mediaUpdateMediator = mediaUpdateMediator)

  // Security

  val webhookBearerToken: String = configuration.get[String]("security.token")

  // Startup

  Await.result(mediaUpdateMediator.update, 1.minute)
  val updateController =
    new UpdateController(
      cc = controllerComponents,
      bearerToken = webhookBearerToken,
      mediaUpdateMediator = mediaUpdateMediator)
  val webhookController =
    new WebhookController(
      cc = controllerComponents,
      bearerToken = webhookBearerToken,
      webhookService = webhookService)

  override def router: Router = new Routes(
    httpErrorHandler,
    updateController,
    webhookController,
    secureMetricsController,
    healthCheckController
  )

  def both[M1, M2](m: M1 with M2): (M1, M2) = (m, m)

  def hacks(): AlbumTrackReportingReversalHack = {
    case class Hacks(albumArtistSwaps: Seq[String])
    object Hacks {
      implicit val hacksReads: Reads[Hacks] = {
        (JsPath \ "albumArtistSwaps").read[Seq[String]].map(Hacks(_))
      }
      def apply[A](config: Configuration): Hacks = {
        val hacksFile: String = config.get[String]("hacks.path")
        val in = new FileInputStream(hacksFile)
        try {
          Json.parse(in).as[Hacks]
        }
        finally {
          in.close()
        }
      }
    }
    val hacks = Hacks(configuration)
    new SequenceMatchingAlbumTrackReportingReversalHack(hacks.albumArtistSwaps)
  }

  def rooms(removePunctuationService: RemovePunctuationService): RoomsProvider = {
    val roomsFile: String = configuration.get[String]("rooms.path")
    val in = new FileInputStream(roomsFile)
    val roomNamesById: Map[String, String] = try {
      Json.parse(in).as[Map[String, String]]
    }
    finally {
      in.close()
    }
    new StaticRoomsProvider(roomNamesById, removePunctuationService)

  }
}
