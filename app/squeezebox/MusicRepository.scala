package squeezebox

import models.{Album, Favourite, Playlist, Room}

import scala.concurrent.Future

/**
  * An interface to get connectedRooms, albums, favourites and playlists from a music repository.
  **/
trait MusicRepository {

  /**
    * Get a list of all known albums, favourites and playlists.
    * @return A list of all known albums, favourites and playlists.
    */
  def everything(): Future[(Set[Album], Set[Favourite], Set[Playlist])]

}
