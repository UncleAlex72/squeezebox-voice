package squeezebox
import lexical.{RemovePunctuationService, SynonymService}
import models._
import play.api.Logger
import uk.co.unclealex.squeezebox.models.{NowPlaying, Player}
import uk.co.unclealex.squeezebox.{MediaServerClient, SqueezeboxProgram => SP}

import scala.concurrent.{ExecutionContext, Future}

/**
  * The default implementation of [[MusicPlayer]] and [[MusicRepository]]
  * Created by alex on 24/12/17
  **/
class MusicPlayerImpl(
                       mediaServerClient: MediaServerClient,
                       synonymService: SynonymService,
                       removePunctuationService: RemovePunctuationService)(implicit ec: ExecutionContext)
  extends MusicPlayer with MusicRepository {

  override def everything(): Future[(Set[Album], Set[Favourite], Set[Playlist])] = {
    mediaServerClient.execute {
      for {
        albums <- SP.albums()
        favourites <- SP.favourites()
        playlists <- SP.playlists()
      } yield (albums, favourites, playlists)
    }.map {
      case (mAlbums, mFavourites, mPlaylists) =>
        val albums: Set[Album] = mAlbums.groupBy(_.title).toSeq.map {
          case (title, protos) =>
            val artists: Seq[Artist] = protos.map { proto =>
              val artist: String = proto.artist
              Artist(artist, entryOf(artist))
            }
            Album(title, artists.toSet, entryOf(title))
        }.toSet
        Logger.info(s"Found ${albums.size} albums.")
        val favourties: Set[Favourite] = mFavourites.map { mFavourite =>
          Favourite(mFavourite.id, mFavourite.name, entryOf(mFavourite.name))
        }.toSet
        Logger.info(s"Found ${favourties.size} favourites.")
        val playlists: Set[Playlist] = mPlaylists.map { mPlaylist =>
          Playlist(mPlaylist.id, mPlaylist.name, mPlaylist.url, entryOf(mPlaylist.name))
        }.toSet
        Logger.info(s"Found ${playlists.size} playlists.")
        (albums, favourties, playlists)
    }
  }

  override def nowPlaying(room: Room): Future[Option[NowPlaying]] = {
    val program: SP[Option[NowPlaying]] = for {
      players <- SP.players()
      maybeNowPlaying <- SP.flatFilter(SP.nowPlaying(room.id), players.exists(player => player.playerId == room.id))
    } yield {
      maybeNowPlaying
    }
    mediaServerClient.execute(program)
  }

  /**
    * Get a list of all connected players.
    *
    * @return A list of all known players.
    */
  override def connectedRooms(): Future[Set[Room]] = {
    val eventualPlayers: Future[Seq[Player]] = mediaServerClient.execute(SP.players())
    eventualPlayers.map { players =>
      players.map(player => Room(player.playerId, player.name, entryOf(player.name))).toSet
    }
  }

  def entryOf(name: String): Entry = {
    val unpunctuatedName = removePunctuationService(name)
    Entry(unpunctuatedName, synonymService(unpunctuatedName))
  }

  def executeAndIgnore(program: SP[_]): Future[Unit] = {
    mediaServerClient.execute(program).map(_ => {})
  }

  /**
    * Play an album on a player.
    *
    * @param player The player that will play the album.
    * @param album  The album to play.
    * @param artist The artist of the album
    * @return Unit.
    */
  override def playAlbum(player: Room, album: Album, artist: Artist): Future[Unit] = {
    executeAndIgnore(SP.playAlbum(player.id, album.title, artist.name))
  }

  /**
    * Play a favourite on a player.
    *
    * @param player    The player that will play the album.
    * @param favourite The favourite to play.
    * @return Unit.
    */
  override def playFavourite(player: Room, favourite: Favourite): Future[Unit] = {
    executeAndIgnore(SP.playFavourite(player.id, favourite.id))
  }

  /**
    * Play a playlist on a player.
    *
    * @param player    The player that will play the album.
    * @param playlist The playlist to play.
    * @return Unit.
    */
  override def playPlaylist(player: Room, playlist: Playlist): Future[Unit] = {
    executeAndIgnore(SP.playPlaylist(player.id, playlist.url, playlist.name))
  }
}
